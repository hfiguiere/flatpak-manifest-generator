// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_use]
extern crate lazy_static;

mod config;
mod flathub;
mod manifest;
mod metainfo;
mod module;
mod tui;

use tui::Prompt;

fn main() {
    if let Some(manifest) = manifest::Manifest::prompt() {
        println!("Result: {:?}", &manifest);
        manifest.generate().expect("Manifest generation");
    }
}
