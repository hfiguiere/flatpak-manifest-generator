flatpak-manifest-generator


This command line tool will generate a manifest after asking you
questions.

You need Rust to build.

`cargo run` will run the tool.

See LICENSES/ for the licensing (it's GPL-3.0-or-later)

